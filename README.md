# oldutil

![](https://img.shields.io/badge/written%20in-VB6%2C%20VB.NET%2C%20C%23%2C%20and%20C%2B%2B-blue)

37 older projects, mostly dating from around 2006-2007.

- 4get - bulk image downloader for the website 4chan.org
- alt - a scriptable 3D RPG game
- aren - batch regular expression renaming utility
- bmphdr - write raw BMP header files to view framebuffer dumps
- cowsay - rewrite of the classic utility
- csprofiler - GUI tool to manage saved games for the game Cave Story
- deciphe - polyalphabetic substitution cipher solver
- dva1 - change view mode of desktop icons
- dva2 - change view mode of desktop icons (Vista+ dependency)
- encoder - bulk video encoder
- erb - CLI tool to empty recycle bin
- exh - launch a program at a specific window position (intended for CLI use)
- expsave - kill explorer by hotkey, used for escaping certain error conditions
- fibs - comical fibbonaci implementation with excessive commenting, using the closed form expression with a field extension over √2
- fvtool - autoclicker for the game Farmville
- hlml - mod launcher for the game Halflife
- httppy - web server with integrated VBS server-side script engine
- intchk - Churuya-san tells you if the internet is working
- lilly-vb - pixel-accurate clone of the Billy media player
- lsau - enumerate windows audio devices in a manner suitable for mpd-win32
- mapp - produce a popup context menu, used to simplify shortcuts
- mappydc - GUI NMDC client with scripting engine and avatar support
- mcparse - parse Minecraft log files for further processing
- mhokay - configure global hotkeys
- multisnake - multiplayer snake game with up to 4 players and AI support
- qemgui - GUI for QEMU
- rcbass - simulate global hotkeys, to control the Billy player over a telnet/ssh connection
- rotype - ROT13 notepad
- slingshotcsv - parse the slingshot API to CSV files
- stepview - manager utility for the game Stepmania
- vb-helpers - a collection of utility scripts for VB6
- wimutil - utility for packing and unpacking .wim files
- wmvappendgui - GUI to losslessly append .wmv files
- xini - patch utility for the program XFire
- yatwiki - early version of a markdown-based wiki script
- yctl - Application framework with GUI and VBS-based scripting engine
- ysis - Event framework for porting TI-83-based applications to Windows

Tags: nmdc, game


## Download

- [⬇️ oldutil.rar](dist-archive/oldutil.rar) *(9.62 MiB)*
